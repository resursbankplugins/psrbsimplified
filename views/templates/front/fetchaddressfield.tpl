<!--
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
-->

{if $hasCredentials && ($shopCountry === 'se' || $shopCountry === 'no')}
    <div class="psrbsimplified-fetch-address" id="psrbsimplified-fetch-address">
        <div class="psrbsimplified-fetch-address-radio-list">
            <label>
                <input
                        type="radio"
                        name="prsbsimplifiedCustomerType"
                        value="person">
                <span>{l s='Private person' mod='psrbsimplified'}</span>
            </label>
            <label>
                <input
                        type="radio"
                        name="prsbsimplifiedCustomerType"
                        value="company">
                <span>{l s='Company' mod='psrbsimplified'}</span>
            </label>
        </div>
        <label
                for="psrbsimplified-fetch-address-input"
                id="psrbsimplified-fetch-address-input-label"
        >
        </label>
        <div>
            <input
                type="text"
                id="psrbsimplified-fetch-address-input"
                class="form-control"
                name="psrbsimplified-fetch-address"
                value=""
            >
            <div class="psrbsimplified-field-error">
                <span id="psrbsimplified-fetch-address-error"></span>
            </div>
        </div>
        <button
                id="psrbsimplified-btn-fetch-address"
                type="button"
                class="">
            <span>{l s='Fetch address' mod='psrbsimplified'}</span>
        </button>
    </div>
{/if}
