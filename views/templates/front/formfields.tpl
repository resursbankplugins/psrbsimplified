<!--
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
-->
<div {if ($fieldVisible || empty($fieldValue) && !$hideField)}style="display: grid;grid-template: auto / 10em 1fr;"{else}style="display: none; grid-template: auto / 10em 1fr;"{/if}>
  <dt class="psrbsimplified-method-dt-label">{$fieldLabel}</dt>
  <dd class="psrbsimplified-method-dd-input-wrapper">
    <input
      type="{if ($fieldVisible || empty($fieldValue) && !$hideField)}text{else}hidden{/if}"
      id="{$fieldName}"
      size="{$fieldSize}"
      class="field_{$fieldName} form-control"
      name="{$fieldName}"
      value="{$fieldValue}">
    <div class="psrbsimplified-method-field-error-message"></div>
    <input type="hidden" name="method" value="{$methodDesc}">
    <input type="hidden" name="id" value="{$id}">
    <input type="hidden" name="shopId" value="{$shopId}">
    <input type="hidden" name="billingAndShippingIsSame" value="{$billingAndShippingIsSame}">
    <input type="hidden" name="isCompany" value="{$isCompany}">
  </dd>
</div>
