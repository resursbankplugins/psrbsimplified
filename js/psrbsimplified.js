/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @namespace RbsModel
 */

/**
 * @namespace RbsView
 */

/**
 * @namespace RbsLib
 */

/**
 * Global variable.
 *
 * @typedef {object} psrbsimplified
 * @property {psrbsimplified.View} View
 * @property {psrbsimplified.Lib} Lib
 * @property {psrbsimplified.Model} Model
 * @property {string} defaultCountryId
 * @property {string} fetchAddressUrl
 * @property {string} removeAddressUrl
 * @property {string} setSessionUrl
 * @property {string|null} govId
 * @property {string|null} fetchedAddress
 */

/**
 * @typedef {object} psrbsimplified.View
 * @property {RbcView.ReadMoreFn} ReadMore
 * @property {RbsView.Method} Method
 * @property {RbsView.PlaceOrder} PlaceOrder
 */

/**
 * @typedef {object} psrbsimplified.Model
 * @property {RbsModel.Checkout} Checkout
 */

/**
 * @typedef {object} psrbsimplified.Lib
 * @property {RbsLib.Checkout} Checkout
 * @property {RbsLib.Storage} Storage
 * @property {RbsLib.Session} Session
 * @property {RbsLib.CheckoutConfig} CheckoutConfig
 * @property {RbsLib.FetchAddress} FetchAddress
 * @property {RbsLib.Credentials} Credentials
 */

if (window.psrbsimplified === undefined) {
    window.psrbsimplified = {};
}

if (window.psrbsimplified.Lib === undefined) {
    window.psrbsimplified.Lib = {};
}

if (window.psrbsimplified.Model === undefined) {
    window.psrbsimplified.Model = {};
}

if (window.psrbsimplified.View === undefined) {
    window.psrbsimplified.View = {};
}
