/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @type {RbsModel.Checkout}
 */
window.psrbsimplified.Model.Checkout =
    (function () {
        'use strict';

        /**
         * @constant
         * @type {object}
         */
        var PRIVATE = {
            /**
             * @type {boolean}
             */
            isCompany: false,

            /**
             * @type {string}
             */
            govId: '',

            /**
             * @type {string}
             */
            contactId: '',

            /**
             * @type {string}
             */
            phone: '',

            /**
             * @type {string}
             */
            country: ''
        };

        /**
         * @namespace RbsModel.Checkout
         * @readonly
         * @constant
         */
        var EXPORT = {
            /**
             * @param {boolean} [value]
             * @returns {boolean}
             */
            isCompany: function (value) {
                if (typeof value === 'boolean') {
                    PRIVATE.isCompany = value;
                }

                return PRIVATE.isCompany;
            },

            /**
             * @param {string} [value]
             * @returns {string}
             */
            govId: function (value) {
                if (typeof value === 'string') {
                    PRIVATE.govId = value;
                }

                return PRIVATE.govId;
            },

            /**
             * @param {string} [value]
             * @returns {string}
             */
            contactId: function (value) {
                if (typeof value === 'string') {
                    PRIVATE.contactId = value;
                }

                return PRIVATE.contactId;
            },

            /**
             * @param {string} [value]
             * @returns {string}
             */
            phone: function (value) {
                if (typeof value === 'string') {
                    PRIVATE.phone = value;
                }

                return PRIVATE.phone;
            },

            /**
             * @param {string} [value]
             * @returns {string}
             */
            country: function (value) {
                if (typeof value === 'string') {
                    PRIVATE.country = value;
                }

                return PRIVATE.country;
            }
        };

        return Object.freeze(EXPORT);
    }());
