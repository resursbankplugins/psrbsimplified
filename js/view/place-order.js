/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * Intercepts the place order submit, and prevents it if we cannot validate
 * the customer data.
 */
window.psrbsimplified.View.PlaceOrder = (function () {
    /**
     * @type {HTMLButtonElement|undefined}
     * @readonly
     */
    var placeOrderBtn;

    /**
     * @type {RbsLib.Credentials}
     * @readonly
     */
    var CredentialsLib;

    /**
     * @type {RbsModel.Checkout}
     * @readonly
     */
    var CheckoutModel;

    /**
     * @type {boolean}
     */
    var initialized = false;

    /**
     * Returns the checked payment method radio button.
     *
     * @returns {jQuery}
     */
    function getPaymentMethodInput() {
        return $('input[name="payment-option"]').filter(':checked');
    }

    /**
     * Returns a list of what customer details should be validated for the
     * selected payment method.
     *
     * @returns {string[]}
     */
    function getValidationTypes() {
        var input = getPaymentMethodInput();
        var validationTypes = [
            'applicant_government_id',
            'contact_government_id',
            'government_id',
            'phone'
        ];
        var result = [];

        if (input.length === 1) {
            // Uses the id of the radio input to find the customer details
            // inputs.
            result = $('#pay-with-' + input.attr('id') + '-form input[type="text"]')
                .map(function (i, el) {
                    var validationType;

                    $.each(validationTypes, function (i, type) {
                        if (el.id.indexOf(type) === 0) {
                            validationType = type;

                            return false;
                        }
                    });

                    return validationType;
                }).get();
        }

        return result;
    }

    /**
     * @returns {boolean}
     */
    function validate() {
        var country = CheckoutModel.country();
        var types = getValidationTypes();
        
        // All results defaults to true if their validation type isn't present.
        var govIdResult = CheckoutModel.isCompany() ?
            $.inArray('applicant_government_id', types) === -1 :
            $.inArray('government_id', types) === -1;

        var contactIdResult = CheckoutModel.isCompany() ?
            $.inArray('contact_government_id', types) === -1 :
            true;

        var phoneResult = $.inArray('phone', types) === -1;

        if (!govIdResult) {
            govIdResult = CredentialsLib.validate(
                CheckoutModel.govId(),
                country,
                CheckoutModel.isCompany()
            );
        }

        if (!contactIdResult) {
            contactIdResult = CredentialsLib.validate(
                CheckoutModel.contactId(),
                country,
                false
            );
        }

        if (!phoneResult) {
            phoneResult = CredentialsLib.validatePhone(
                CheckoutModel.phone(),
                country
            );
        }

        return contactIdResult && govIdResult && phoneResult;
    }

    return {
        init: function () {
            if (!initialized) {
                placeOrderBtn = $(
                    '#payment-confirmation button[type="submit"]'
                )[0];
                CredentialsLib = window.psrbsimplified.Lib.Credentials;
                CheckoutModel = window.psrbsimplified.Model.Checkout;

                if (placeOrderBtn instanceof HTMLButtonElement) {
                    placeOrderBtn.addEventListener('click', function (event) {
                        if (!validate()) {
                            event.preventDefault();
                            event.stopPropagation();
                            event.stopImmediatePropagation();
                        }
                    });
                }

                initialized = true;
            }
        }
    };
}());
