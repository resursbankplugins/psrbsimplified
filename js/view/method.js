/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @param {object} data
 * @param {string} data.formId
 */
window.psrbsimplified.View.Method = function (data) {
    /**
     * @type {RbsLib.Credentials}
     * @readonly
     */
    var CredentialsLib = window.psrbsimplified.Lib.Credentials;

    /**
     * @type {RbsModel.Checkout}
     * @readonly
     */
    var CheckoutModel = window.psrbsimplified.Model.Checkout;

    /**
     * Beginning portion of the government id input.
     *
     * @type {string}
     */
    var govIdInputId = 'government_id';

    /**
     * Beginning portion of the contact government id input.
     *
     * @type {string}
     */
    var contactGovIdInputId = 'contact_government_id';

    /**
     * Beginning portion of the applicant government id input.
     *
     * @type {string}
     */
    var applicantGovIdInputId = 'applicant_government_id';

    /**
     * Beginning portion of the phone input.
     *
     * @type {string}
     */
    var phoneInputId = 'phone';

    /**
     * @type {HTMLInputElement}
     * @readonly
     */
    var phoneInput = $(
        '#' + data.formId + ' input[id^="' + phoneInputId + '"]'
    )[0];

    /**
     * @type {HTMLInputElement}
     * @readonly
     */
    var govIdInput = $(
        '#' + data.formId + ' input[id^="' + govIdInputId + '"]'
    )[0];

    /**
     * @type {HTMLInputElement}
     * @readonly
     */
    var contactGovIdInput = $(
        '#' + data.formId + ' input[id^="' + contactGovIdInputId + '"]'
    )[0];

    /**
     * @type {HTMLInputElement}
     * @readonly
     */
    var applicantGovIdInput = $(
        '#' + data.formId + ' input[id^="' + applicantGovIdInputId + '"]'
    )[0];

    /**
     * @type {HTMLDivElement}
     * @readonly
     */
    var phoneErrorMessageEl = $(
        '#' + data.formId + ' input[id^="' + phoneInputId + '"]+' +
        '.psrbsimplified-method-field-error-message'
    )[0];

    /**
     * @type {HTMLDivElement}
     * @readonly
     */
    var govIdErrorMessageEl = $(
        '#' + data.formId + ' input[id^="' + govIdInputId + '"]+' +
        '.psrbsimplified-method-field-error-message'
    )[0];

    /**
     * @type {HTMLDivElement}
     * @readonly
     */
    var contactGovIdErrorMessageEl = $(
        '#' + data.formId + ' input[id^="' + contactGovIdInputId + '"]+' +
        '.psrbsimplified-method-field-error-message'
    )[0];

    /**
     * @type {HTMLDivElement}
     * @readonly
     */
    var applicantGovIdErrorMessageEl = $(
        '#' + data.formId + ' input[id^="' + applicantGovIdInputId + '"]+' +
        '.psrbsimplified-method-field-error-message'
    )[0];

    /**
     * @returns {boolean}
     */
    function validatePhone() {
        return CredentialsLib.validatePhone(
            CheckoutModel.phone(),
            CheckoutModel.country()
        );
    }

    /**
     * @returns {boolean}
     */
    function validateGovId() {
        return CredentialsLib.validate(
            CheckoutModel.govId(),
            CheckoutModel.country(),
            CheckoutModel.isCompany()
        );
    }

    /**
     * @returns {boolean}
     */
    function validateContactGovId() {
        return CredentialsLib.validate(
            CheckoutModel.contactId(),
            CheckoutModel.country(),
            false
        );
    }

    /**
     * Sets the error message and marks the government id input as faulty.
     */
    function updateGovIdError() {
        if (!validateGovId()) {
            govIdErrorMessageEl.textContent = window.psrbsimplified['translations']['invalidGovernmentId'];
            govIdInput.classList.add('psrbsimplified-method-field-error');
        } else {
            govIdErrorMessageEl.textContent = '';
            govIdInput.classList.remove('psrbsimplified-method-field-error');
        }
    }

    /**
     * Sets error message and marks the contact government id input as faulty.
     */
    function updateContactGovIdError() {
        if (!validateContactGovId()) {
            contactGovIdErrorMessageEl.textContent = window.psrbsimplified['translations']['invalidGovernmentId'];
            contactGovIdInput.classList.add('psrbsimplified-method-field-error');
        } else {
            contactGovIdErrorMessageEl.textContent = '';
            contactGovIdInput.classList.remove('psrbsimplified-method-field-error');
        }
    }

    /**
     * Sets error message and marks the applicant government id input as faulty.
     */
    function updateApplicantGovIdError() {
        if (!validateGovId()) {
            applicantGovIdErrorMessageEl.textContent = window.psrbsimplified['translations']['invalidOrganizationNumber'];
            applicantGovIdInput.classList.add('psrbsimplified-method-field-error');
        } else {
            applicantGovIdErrorMessageEl.textContent = '';
            applicantGovIdInput.classList.remove('psrbsimplified-method-field-error');
        }
    }

    /**
     * Sets the error message and marks the phone input as faulty.
     */
    function updatePhoneError() {
        if (!validatePhone()) {
            phoneErrorMessageEl.textContent = window.psrbsimplified['translations']['invalidPhoneNumber'];
            phoneInput.classList.add('psrbsimplified-method-field-error');
        } else {
            phoneErrorMessageEl.textContent = '';
            phoneInput.classList.remove('psrbsimplified-method-field-error');
        }
    }

    /**
     * Event handler for when the phone input changes.
     */
    function onPhoneChange() {
        CheckoutModel.phone(phoneInput.value);
        updatePhoneError();
    }

    /**
     * Event handler for when the government id input changes.
     */
    function onGovIdChange() {
        CheckoutModel.govId(govIdInput.value);
        updateGovIdError();
    }

    /**
     * Event handler for when applicant the government id input changes.
     */
    function onApplicantGovIdChange() {
        CheckoutModel.govId(applicantGovIdInput.value);
        updateApplicantGovIdError();
    }

    /**
     * Event handler for when the contact government id input changes.
     */
    function onContactGovIdChange() {
        CheckoutModel.contactId(contactGovIdInput.value);
        updateContactGovIdError();
    }

    /**
     * Updates the phone field value to the one that's stored in the model.
     */
    function updatePhoneFieldValue() {
        if (phoneInput instanceof HTMLInputElement) {
            phoneInput.value = CheckoutModel.phone();
        }
    }

    /**
     * Updates the govId field value to the one that's stored in the model.
     */
    function updateGovIdFieldValue() {
        if (govIdInput instanceof HTMLInputElement) {
            govIdInput.value = CheckoutModel.govId();
        }
    }

    /**
     * Updates the applicantGovId field value to the one that's stored in the
     * model.
     */
    function updateApplicantGovIdValue() {
        if (applicantGovIdInput instanceof HTMLInputElement) {
            applicantGovIdInput.value = CheckoutModel.govId();
        }
    }

    /**
     * Updates the contactGovId field value to the one that's stored in the
     * model.
     */
    function updateContactGovIdValue() {
        if (contactGovIdInput instanceof HTMLInputElement) {
            contactGovIdInput.value = CheckoutModel.contactId();
        }
    }

    /**
     * Updates all field values to the ones stored in the model.
     */
    function updateFieldValues() {
        updatePhoneFieldValue();
        updateGovIdFieldValue();
        updateApplicantGovIdValue();
        updateContactGovIdValue();
    }

    /**
     * Displays both the phone field and its parent block.
     */
    function displayPhoneField() {
        phoneInput.type = 'text';
        phoneInput.parentNode.parentNode.style.display = 'grid';
    }

    if (phoneInput instanceof HTMLInputElement &&
        phoneErrorMessageEl instanceof HTMLDivElement
    ) {
        phoneInput.addEventListener('change', onPhoneChange);

        // Validation when page has loaded.
        if (CheckoutModel.phone() === '' || !validatePhone()) {
            if (CheckoutModel.phone() !== '') {
                updatePhoneError();
            }

            // The phone field is always hidden, but we need to display it if
            // it has no value or if it's invalid.
            displayPhoneField();
        }
    }

    if (govIdInput instanceof HTMLInputElement &&
        govIdErrorMessageEl instanceof HTMLDivElement &&
        govIdInput.type !== 'hidden'
    ) {
        govIdInput.addEventListener('change', onGovIdChange);

        // Validation when page has loaded.
        if (CheckoutModel.govId() !== '') {
            updateGovIdError();
        }
    }

    if (contactGovIdInput instanceof HTMLInputElement &&
        contactGovIdErrorMessageEl instanceof HTMLDivElement &&
        contactGovIdInput.type !== 'hidden'
    ) {
        contactGovIdInput.addEventListener('change', onContactGovIdChange);

        // Validation when page has loaded.
        if (CheckoutModel.contactId() !== '') {
            updateContactGovIdError();
        }
    }

    if (applicantGovIdInput instanceof HTMLInputElement &&
        applicantGovIdErrorMessageEl instanceof HTMLDivElement &&
        applicantGovIdInput.type !== 'hidden'
    ) {
        applicantGovIdInput.addEventListener('change', onApplicantGovIdChange);

        // Validation when page has loaded.
        if (CheckoutModel.govId() !== '') {
            updateApplicantGovIdError();
        }
    }

    $('input[name="payment-option"]').on('change', function () {
        updateFieldValues();

        if (govIdInput instanceof HTMLInputElement &&
            govIdInput.value !== ''
        ) {
            updateGovIdError();
        }

        if (phoneInput instanceof HTMLInputElement &&
            phoneInput.value !== ''
        ) {
            updatePhoneError();
        }

        if (contactGovIdInput instanceof HTMLInputElement &&
            contactGovIdInput.value !== ''
        ) {
            updateContactGovIdError();
        }

        if (applicantGovIdInput instanceof HTMLInputElement &&
            applicantGovIdInput.value !== ''
        ) {
            updateApplicantGovIdError();
        }
    });
};
