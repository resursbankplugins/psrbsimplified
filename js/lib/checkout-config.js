/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @type RbsLib.CheckoutConfig
 */
window.psrbsimplified.Lib.CheckoutConfig = (function () {
    'use strict';

    /**
     * @constant
     * @readonly
     * @namespace RbsLib.CheckoutConfig
     */
    var EXPORT = {
        /**
         * Returns the government id that was used to fetch an address.
         *
         * @returns {string} An empty string will be returned if a value
         * couldn't be found.
         */
        getGovId: function() {
            return window.hasOwnProperty('psrbsimplified') &&
            typeof window.psrbsimplified.govId === 'string' ?
                window.psrbsimplified.govId :
                '';
        },

        /**
         * Returns the fetched address.
         *
         * @returns {string|RbsLib.FetchAddress.Address} An empty string will
         * be returned if a fetched address couldn't be found.
         */
        getFetchedAddress: function () {
            return window.hasOwnProperty('psrbsimplified') &&
                typeof window.psrbsimplified.fetchedAddress === 'string' &&
                window.psrbsimplified.fetchedAddress !== '' ?
                    JSON.parse(
                        window.psrbsimplified.fetchedAddress.replace(/&quot;/g, '"')
                    ) : '';
        },

        /**
         * Returns the default country the store is configured to as a
         * valid two-letter ISO 3166 code.
         *
         * @returns {string} An empty string will be returned if a value
         * couldn't be found.
         */
        getDefaultCountryId: function() {
            return window.hasOwnProperty('psrbsimplified') &&
                typeof window.psrbsimplified.defaultCountryId === 'string' ?
                    window.psrbsimplified.defaultCountryId :
                    '';
        },

        /**
         * Returns the URL to contact the controller responsible for
         * fetching an address.
         *
         * @returns {string} An empty string will be returned if a value
         * couldn't be found.
         */
        getFetchAddressUrl: function() {
            return window.hasOwnProperty('psrbsimplified') &&
                typeof window.psrbsimplified.fetchAddressUrl === 'string' ?
                    window.psrbsimplified.fetchAddressUrl :
                    '';
        },

        /**
         * Returns the URL to contact the controller responsible for
         * removing a fetched address.
         *
         * @returns {string} An empty string will be returned if a value
         * couldn't be found.
         */
        getRemoveAddressUrl: function() {
            return window.hasOwnProperty('psrbsimplified') &&
            typeof window.psrbsimplified.removeAddressUrl === 'string' ?
                window.psrbsimplified.removeAddressUrl :
                '';
        },

        /**
         * Returns the URL to contact the controller responsible for updating
         * session data.
         *
         * @returns {string} An empty string will be returned if the URL
         * couldn't be found.
         */
        getSetSessionUrl: function() {
            return window.hasOwnProperty('psrbsimplified') &&
            typeof window.psrbsimplified.setSessionUrl === 'string' ?
                window.psrbsimplified.setSessionUrl :
                '';
        }
    };

    return Object.freeze(EXPORT);
}());
