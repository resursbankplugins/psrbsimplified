/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
/**
 * @type {RbsLib.Checkout}
 */
psrbsimplified.Lib.Checkout =
    /**
     * @param {jQuery} $
     * @returns RbsLib.Checkout
     */
    (function (
        $
    ) {
        'use strict';

        /**
         * @typedef {object} RbsLib.Checkout.AddressInputs
         * @property {jQuery} firstname
         * @property {jQuery} lastname
         * @property {jQuery} company
         * @property {jQuery} street0
         * @property {jQuery} street1
         * @property {jQuery} postcode
         * @property {jQuery} country
         * @property {jQuery} city
         * @property {jQuery} telephone
         */

        /**
         * Will return the address section element for the current checkout
         * step.
         *
         * Form inputs for the customer's first & last name exists in both
         * first and second steps of the checkout. When fetching address on the
         * first step, the inputs on the second step cannot be updated since
         * those inputs do not yet exist in the DOM.
         *
         * @returns {jQuery}
         */
        function getCurrentAddressSection() {
            var step2 = $(
                '#checkout-addresses-step:not(.-unreachable) #delivery-address'
            );
            var step1 = $('#checkout-guest-form');

            return step2.length > 0 ? step2 : step1;
        }

        /**
         * @constant
         * @readonly
         * @namespace RbsLib.Checkout
         */
        var EXPORT = {
            /**
             * Returns the JS-components of the input fields in the simplified
             * checkout.
             *
             * @returns {RbsLib.Checkout.AddressInputs}
             */
            getAddressInputs: function() {
                var addressSection = getCurrentAddressSection();

                return {
                    firstname: addressSection.find('input[name="firstname"]'),
                    lastname: addressSection.find('input[name="lastname"]'),
                    company: addressSection.find('input[name="company"]'),
                    street0: addressSection.find('input[name="address1"]'),
                    street1: addressSection.find('input[name="address2"]'),
                    postcode: addressSection.find('input[name="postcode"]'),
                    country: addressSection.find('select[name="id_country"]'),
                    city: addressSection.find('input[name="city"]'),
                    telephone: addressSection.find('input[name="phone"]')
                };
            },

            /**
             * Applies a fetched shipping address to the shipping address
             * input fields in the simplified checkout.
             *
             * @param {RbsLib.FetchAddress.Address} address
             * @return {RbsLib.Checkout}
             */
            applyAddress: function(address) {
                var inputs = EXPORT.getAddressInputs();

                // Content validation on first- and lastname applies on LEGAL
                // requests.
                if (address.firstname !== '') {
                    inputs.firstname.val(address.firstname);
                }

                if (address.lastname !== '') {
                    inputs.lastname.val(address.lastname);
                }

                inputs.city.val(address.city);
                inputs.postcode.val(address.postcode);
                inputs.street0.val(address.street0);
                inputs.street1.val(address.street1);
                inputs.country.val(address.country);
                inputs.company.val(address.company);
                inputs.telephone.val(address.telephone);
                inputs.country.val(address.country);

                if (inputs.country.length > 0 && address.countryId > 0) {
                    inputs.country
                        .find('option[value="' + address.countryId + '"]')
                        .prop('selected', true);
                }

                return EXPORT;
            },

            /**
             * Removes an applied shipping address from the input fields of
             * the simplified checkout. The input fields will have their
             * initial values restored.
             *
             * @return {RbsLib.Checkout}
             */
            removeAddress: function() {
                var inputs = EXPORT.getAddressInputs();

                inputs.firstname.val('');
                inputs.lastname.val('');
                inputs.city.val('');
                inputs.postcode.val('');
                inputs.street0.val('');
                inputs.street1.val('');
                inputs.country.val('');
                inputs.company.val('');
                inputs.telephone.val('');

                return EXPORT;
            }
        };

        return Object.freeze(EXPORT);
    }(
        $
    ));
