/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
/**
 * @type {RbsLib.Storage}
 */
psrbsimplified.Lib.Storage =
    /**
     * @param {jQuery} $
     * @returns RbsLib.Storage
     */
    (function (
        $
    ) {
        'use strict';

        var prefix = 'psrbsimplified';
        var isCompanyKey = prefix + '.isCompany';

        /**
         * @constant
         * @readonly
         * @namespace RbsLib.Storage
         */
        var EXPORT = {
            /**
             * @param {boolean} isCompany
             */
            setIsCompany: function (isCompany) {
                if (typeof isCompany === 'boolean') {
                    window.sessionStorage.setItem(
                        isCompanyKey,
                        JSON.stringify(isCompany)
                    );
                }
            },

            /**
             * @return {boolean|undefined}
             */
            getIsCompany: function () {
                return JSON.parse(window.sessionStorage.getItem(isCompanyKey));
            }
        };

        return Object.freeze(EXPORT);
    }(
        $
    ));
