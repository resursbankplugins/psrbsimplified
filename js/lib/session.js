/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @type RbsLib.Session
 */
window.psrbsimplified.Lib.Session =
    /**
     * @param {jQuery} $
     * @param {RbsLib.CheckoutConfig} CheckoutConfig
     * @returns RbsLib.Session
     */
    (function (
        $,
        CheckoutConfig
    ) {
        'use strict';

        /**
         * @typedef {object} RbsLib.Session.Error
         * @property {string} message
         */

        /**
         * @typedef {object} RbsLib.Session.Call
         * @property {string} type
         * @property {string} url
         * @property {object} data
         * @property {boolean} data.is_company
         * @property {boolean} data.ajax
         */

        /**
         * @typedef {object} RbsLib.Session.Response
         * @property {RbsLib.Session.Error} error
         */

        /**
         * @constant
         * @readonly
         * @namespace RbsLib.Session
         */
        var EXPORT = {
            /**
             * Sends a request to the server that sets the given information
             * in the session for later use.
             *
             * @param {boolean} isCompany
             * @return {jQuery}
             */
            setSessionData: function (isCompany) {
                return $.ajax(EXPORT.getSetSessionCall(
                    isCompany
                ));
            },

            /**
             * Produces a call object which can make a request to apply data in
             * the PHP session.
             *
             * @param {boolean} isCompany
             * @returns {RbsLib.Session.Call}
             */
            getSetSessionCall: function (isCompany) {
                return {
                    type: 'POST',
                    dataType: 'json',
                    url: CheckoutConfig.getSetSessionUrl(),
                    data: {
                        ajax: true,
                        is_company: isCompany
                    }
                };
            }
        };

        return Object.freeze(EXPORT);
    }(
        $,
        window.psrbsimplified.Lib.CheckoutConfig
    ));
