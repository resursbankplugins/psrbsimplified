<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
/* @noinspection PhpCSValidationInspection */

declare(strict_types=1);

/* Start our custom atuoloader. We must start it at this point for the module
 manager to function. */
require_once(_PS_MODULE_DIR_ . "/psrbcore/src/Service/Autoloader.php");

use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Exception\InstallerException;
use Resursbank\Core\Exception\MissingPathException;
use Resursbank\Core\Exception\PaymentMethodException;
use Resursbank\Core\ModuleInterface;
use Resursbank\Core\Traits\Module\Init;
use Resursbank\Core\Traits\Module\Validate;
use Resursbank\Simplified\Service\Checkout;
use Resursbank\Simplified\Service\Session;
use TorneLIB\Exception\ExceptionHandler;

// Make sure the file is loaded within the context of PrestaShop.
if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Resurs Bank simplified flow module.
 *
 * @since 1.0.0
 */
class psrbsimplified extends PaymentModule implements ModuleInterface
{
    use Init;
    use Validate;

    /**
     * List of hooks (event observers) to register.
     *
     * @var string[]
     */
    private $hooks = [
        'paymentOptions',
        'displayPersonalInformationTop',
        'actionFrontControllerSetMedia',
    ];

    /**
     * @throws ExceptionHandler
     * @throws ReflectionException
     * @throws MissingPathException
     */
    public function __construct()
    {
        $this->init($this);
        parent::__construct();
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public static function templatePath(): string
    {
        return sprintf('%s/views/templates/front', self::getPluginPath());
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public static function getPluginPath(): string
    {
        return __DIR__;
    }

    /**
     * @return Context
     *
     * @since 1.0.0
     */
    public function getContext(): Context
    {
        return $this->context;
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return string
     *
     * @since 1.0.0
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     *
     * @throws InstallerException
     *
     * @since 1.0.0
     */
    public function install(): bool
    {
        try {
            // This module cannot function without psrbcore.
            $this->validateCore();

            if (!parent::install()) {
                throw new Exception('Parent installation procedure failed.');
            }
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function uninstall(): bool
    {
        try {
            if (!parent::uninstall()) {
                throw new Exception('Parent uninstall procedure failed.');
            }
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function enable($force_all = false): bool
    {
        try {
            // This module cannot function without psrbcore.
            $this->validateCore();

            if (!parent::enable($force_all)) {
                throw new Exception('Parent enable procedure failed.');
            }

            // Register event observers.
            $this->registerHook($this->hooks);
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function disable($force_all = false): bool
    {
        try {
            if (!parent::disable($force_all)) {
                throw new Exception('Parent disable procedure failed.');
            }

            // Unregister hooks (event observers).
            foreach ($this->hooks as $hook) {
                $this->unregisterHook($hook);
            }
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * Declares we intend to use the new interface to translate our module.
     *
     * @return bool
     * @noinspection PhpMissingParentCallCommonInspection
     *
     * @since 1.0.0
     */
    public function isUsingNewTranslationSystem(): bool
    {
        return true;
    }

    /**
     * @param string $key
     * @param array $params
     *
     * @return string
     *
     * @since 1.0.0
     */
    public function getModuleLink(string $key, array $params = []): string
    {
        return $this->context->link->getModuleLink(
            $this->name,
            $key,
            $params,
            true
        );
    }

    /**
     * @return array
     *
     * @throws SmartyException
     * @throws JsonException
     * @throws PaymentMethodException
     * @throws SmartyException
     * @throws ShopException
     * @throws Exception
     *
     * @since 1.0.0
     * @noinspection PhpUnused
     */
    public function hookPaymentOptions(): array
    {
        $return = [];
        if ($this->context->shop instanceof Shop) {
            /** @var Config $coreConfig */
            $coreConfig = $this->get('resursbank.core.config');
            $selectedFlow = $coreConfig->getFlow(
                ShopConstraint::shop($this->context->shop->id)
            );

            if ($selectedFlow === 'simplified') {
                /** @var Checkout $checkout */
                $checkout = $this->get('resursbank.simplified.checkout');

                $return = $checkout
                    ->setModule($this)
                    ->getPaymentOptions();
            }
        }

        return $return;
    }

    /**
     * @return string
     *
     * @throws SmartyException
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function hookDisplayPersonalInformationTop(): string
    {
        $result = '';

        if (!$this->context->customer->isLogged()) {
            /** @var Checkout $checkout */
            $checkout = $this->get('resursbank.simplified.checkout');

            $result = $checkout
                ->setModule($this)
                ->getFetchAddressField();
        }

        return $result;
    }

    /**
     * Add address fetching and phone validation JS too checkout.
     *
     * @return void
     *
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function hookActionFrontControllerSetMedia(): void
    {
        $self = $this->context->controller->php_self;

        if ($self === 'order') {
            /** @var Session $session */
            $session = $this->get('resursbank.simplified.session');

            $transJsDef = [
                'invalidIdentifierError' => $this->l('Invalid SSN/Org. number.'),
                'invalidPhoneNumber' => $this->l('Invalid phone number.'),
                'invalidGovernmentId' => $this->l('Invalid government id.'),
                'invalidOrganizationNumber' => $this->l('Invalid organization number.'),
                'failedToFetchAddressError' => $this->l('Something went wrong when fetching the address. ' .
                    'Please try again.'),
                'inputLabelCompany' => $this->l('Org. number (XXXXXXXXXX)'),
                'inputLabelPerson' => $this->l('SSN (YYYYMMDDXXXX)'),
                'btnNotYou' => $this->l('Not you?'),
                'btnFetchAddress' => $this->l('Fetch address'),
            ];

            $jsDef = [
                'defaultCountryId' => $this->context->country->iso_code,
                'translations' => $transJsDef,
                'govId' => $session->getGovId() ?: '',
                'isCompany' => $session->getIsCompany() ?: false,
                'fetchedAddress' => $session->getFetchedAddress() ?: '',
            ];

            $this->context->controller->registerJavascript(
                $this->name . '-init',
                $this->getPathUri() . '/js/psrbsimplified.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-storage',
                $this->getPathUri() . '/js/lib/storage.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-view-place-order',
                $this->getPathUri() . '/js/view/place-order.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-view-payment-method',
                $this->getPathUri() . '/js/view/method.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-lib-checkout-config',
                $this->getPathUri() . '/js/lib/checkout-config.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-lib-checkout',
                $this->getPathUri() . '/js/lib/checkout.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-model-checkout',
                $this->getPathUri() . '/js/model/checkout.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-lib-libphonenumber',
                $this->getPathUri() . '/js/lib/libphonenumber.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-lib-credentials',
                $this->getPathUri() . '/js/lib/credentials.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-session',
                $this->getPathUri() . '/js/lib/session.js',
            );

            psrbcore::addRemodalAssets(
                $this->name,
                $this->getPathUri(),
                $this->context->controller
            );

            psrbcore::addReadMoreAssets(
                $this->name,
                $this->getPathUri(),
                $this->context->controller
            );

            $this->context->controller->registerStylesheet(
                $this->name . '-css-fetch-address',
                $this->getPathUri() . '/css/fetch-address.css',
            );

            $this->context->controller->registerStylesheet(
                $this->name . '-css-payment-method',
                $this->getPathUri() . '/css/payment-method.css',
            );

            $jsDef['fetchAddressUrl'] = $this->getModuleLink('fetchaddress');
            $jsDef['removeAddressUrl'] = $this->getModuleLink('removeaddress');
            $jsDef['setSessionUrl'] = $this->getModuleLink('session');

            $this->context->controller->registerJavascript(
                $this->name . '-lib-fetch-address',
                $this->getPathUri() . '/js/lib/fetch-address.js',
            );

            $this->context->controller->registerJavascript(
                $this->name . '-view-fetch-address',
                $this->getPathUri() . '/js/view/fetch-address.js',
            );

            Media::addJsDef(['psrbsimplified' => $jsDef]);
        }
    }
}
