<?php
/*
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use Resursbank\Core\Exception\ApiDataException;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Exception\MissingRequestParameterException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Simplified\Service\Address as AddressService;
use Resursbank\Simplified\Service\Request;
use Resursbank\Simplified\Service\Session;

/**
 * Fetch address from API.
 */
class psrbsimplifiedFetchaddressModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     */
    public function postProcess(): void
    {
        parent::postProcess();
        echo json_encode($this->fetchAddress(), JSON_THROW_ON_ERROR);
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    private function fetchAddress(): array
    {
        /** @var Logger $log */
        $log = $this->get('resursbank.simplified.logger');

        /** @var Session $session */
        $session = $this->get('resursbank.simplified.session');

        $data = [
            'address' => [],
            'error' => [
                'message' => '',
            ],
        ];

        // Resolve customer address.
        /* @noinspection BadExceptionsProcessingInspection */
        try {
            $this->removeAddresses();
            $address = $this->getAddress();
            $session->setGovId($this->getIdentifier());
            $session->setFetchedAddress(
                json_encode($address, JSON_THROW_ON_ERROR)
            );
            $data['address'] = $address;
        } catch (Exception $e) {
            $log->exception($e);

            $errorMessage = 'Something went wrong when fetching the address. Please try again.';

            if ($e->getCode() === RESURS_EXCEPTIONS::UNKOWN_SOAP_EXCEPTION_CODE_ZERO ||
                $e->getCode() === 1019 ||
                $e->getCode() === 1015
            ) {
                $errorMessage = $e->getMessage();
            }

            // Display friendly (safe) error message to customer.
            $data['error']['message'] = $errorMessage;
        }

        return $data;
    }

    /**
     * Will remove all applied addresses so that the address form in step 2 will
     * show up again if the user goes back to step 1 and fetches an address.
     *
     * @return void
     */
    public function removeAddresses(): void
    {
        $langId = $this->context->language->id;
        $addresses = $this->context->customer->getAddresses($langId);

        if (count($addresses) > 0) {
            $token = Tools::getToken(false, $this->context);

            // The CustomerAddressPersister is used internally by Presta to
            // delete addresses.
            $addressPersister = new CustomerAddressPersister(
                $this->context->customer,
                $this->context->cart,
                $token,
            );

            foreach ($addresses as $address) {
                $id = (int) $address['id_address'];

                $addressPersister->delete(
                    new Address(
                        $id,
                        $langId
                    ),
                    $token
                );

                // Each time an address is removed, Presta unsets the address
                // ids on the cart which will make the subsequent deletes
                // throw because they are now undefined. The system is made to
                // only remove one address at the time, then reload the page, so
                // we have to update the ids after each delete so that we can
                // remove multiple addresses at once.
                $this->context->cart->updateAddressId($id, 0);
            }
        }
    }

    /**
     * @return array
     *
     * @throws ApiDataException
     * @throws Exception
     */
    private function getAddress(): array
    {
        /** @var AddressService $addressService */
        $addressService = $this->get('resursbank.simplified.address');

        return $addressService
            ->toCheckoutAddress(
                $addressService->fetch(
                    $this->getIdentifier(),
                    $this->getIsCompany()
                )
            )
            ->toArray();
    }

    /**
     * @return string
     *
     * @throws InvalidDataException
     * @throws MissingRequestParameterException
     * @throws Exception
     */
    private function getIdentifier(): string
    {
        /** @var Request $requestService */
        $requestService = $this->get('resursbank.simplified.request');

        return $requestService->getIdentifier($this->getIsCompany());
    }

    /**
     * @return bool
     *
     * @throws MissingRequestParameterException
     * @throws Exception
     */
    private function getIsCompany(): bool
    {
        /** @var Request $requestService */
        $requestService = $this->get('resursbank.simplified.request');

        return $requestService->isCompany();
    }
}
