<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
/* @noinspection PhpCSValidationInspection */

declare(strict_types=1);

use Resursbank\Simplified\Service\OrderHandler;

/**
 * Order creation class. The naming is based on the principle "Validate cart to order".
 *
 * @since 1.0.0
 */
class psrbsimplifiedValidateModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     * @since 1.0.0
     */
    public function postProcess(): void
    {
        /** @var OrderHandler $orderHandler */
        $orderHandler = $this->get('resursbank.simplified.orderhandler');

        try {
            $orderHandler->validateCartToOrder();
        } catch (Exception $e) {
            // Errors that should be logged despite debugging disabled.
            $orderHandler->logger->exception($e, true);
            $this->errors[] = $e->getMessage();
        }

        if (count($this->errors)) {
            /*
             * getFailUrl here is built to make it possible to redirect to a
             * failurl even if there is a broken cart (I.E. if someone tries to
             * handle an order without a present cart).
             */
            $this->redirectWithNotifications($orderHandler->getFailUrl(true));
        }

        exit;
    }
}
