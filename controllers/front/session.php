<?php
/*
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use Resursbank\Core\Exception\MissingRequestParameterException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Simplified\Service\Request;
use Resursbank\Simplified\Service\Session;

/**
 * Maintain session data.
 */
class psrbsimplifiedSessionModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     */
    public function postProcess(): void
    {
        parent::postProcess();
        echo json_encode($this->updateSession(), JSON_THROW_ON_ERROR);
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    private function updateSession(): array
    {
        $data = [
            'error' => [
                'message' => '',
            ],
        ];

        try {
            /** @var Session $session */
            $session = $this->get('resursbank.simplified.session');
            $session->setIsCompany($this->getIsCompany());
        } catch (Exception $e) {
            /** @var Logger $log */
            $log = $this->get('resursbank.simplified.logger');
            $log->exception($e);

            $data['error']['message'] = 'Couldn\'t save data to session.';
        }

        return $data;
    }

    /**
     * @return bool
     *
     * @throws MissingRequestParameterException
     * @throws Exception
     */
    private function getIsCompany(): bool
    {
        /** @var Request $requestService */
        $requestService = $this->get('resursbank.simplified.request');

        return $requestService->isCompany();
    }
}
