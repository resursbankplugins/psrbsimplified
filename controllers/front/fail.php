<?php

/** @noinspection PhpCSValidationInspection */

/*
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use Resursbank\Simplified\Service\OrderHandler;

/**
 * Order creation class. The naming is based on the principle "Validate cart to order".
 *
 * @since 1.0.0
 */
class psrbsimplifiedfailModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     * @since 1.0.0
     */
    public function postProcess(): void
    {
        /** @var OrderHandler $orderHandler */
        $orderHandler = $this->get('resursbank.simplified.orderhandler');
        $orderHandler->rebuildAndCancel();

        // The intentions of this class is that no code will reach this point. Everything that happens in the
        // rebuild procedure should render a proper redirect. However, if this does not happen for some
        // reason and instead, we receive other internal errors on the way through the flow, the final redirect
        // will be made from here.

        if (count($this->errors)) {
            // getFailUrl here is built to make it possible to redirect to a failurl even if there is a broken
            // cart (I.E. if someone tries to handle an order without a present cart).
            $this->redirectWithNotifications($orderHandler->getFailUrl(true));
        }
        exit;
    }
}
