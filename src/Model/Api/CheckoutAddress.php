<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Model\Api;

use Resursbank\Core\Exception\ApiDataException;
use function strlen;

/**
 * This class is meant to represent a valid address object on the checkout page.
 * It is not meant to replace any of the platforms own address implementations,
 * but rather to help us out when we need to convert address information fetched
 * from the API.
 *
 * The address information that comes from the API is not directly compatible
 * with the platform checkout process (and neither should we expect it to be) as
 * it may not contain the right number of fields, or the fields have names that
 * differ from the platform.
 */
class CheckoutAddress
{
    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $city;

    /**
     * Expected to be formatted like: "12345".
     *
     * @var string
     */
    private $postcode;

    /**
     * Only valid countries are allowed.
     *
     * @var string
     */
    private $country;

    /**
     * Country id, in a format that PrestaShop understand. As for now, the plugin still needs the country
     * ID iso format to validate if getAddress can be used, so we only pass over an integer that we can put
     * into the country field in the checkout.
     *
     * @var int
     *
     * @since 1.0.0
     */
    private $countryId;

    /**
     * @var string
     */
    public $street0;

    /**
     * @var string
     */
    public $street1;

    /**
     * @var string
     */
    public $company;

    /**
     * @var string
     */
    public $telephone;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $city
     * @param string $postcode
     * @param string $country
     * @param string $street0
     * @param string $street1
     * @param string $company
     * @param string $telephone
     * @param int $countryId
     *
     * @throws ApiDataException
     */
    public function __construct(
        string $firstName,
        string $lastName,
        string $city,
        string $postcode,
        string $country,
        string $street0,
        string $street1 = '',
        string $company = '',
        string $telephone = '',
        int $countryId = 0
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->city = $city;
        $this->street0 = $street0;
        $this->street1 = $street1;
        $this->company = $company;
        $this->telephone = $telephone;
        $this->countryId = $countryId;

        $this->setCountry($country)->setPostcode($postcode, $country);
    }

    /**
     * @param string $val
     * @param string $country
     *
     * @return self
     *
     * @see CheckoutAddress::$postcode
     */
    public function setPostcode(
        string $val,
        string $country
    ): self {
        // Presta expects postcodes to be formatted as "123 45" for Sweden.
        if ($country === 'SE' && strlen($val) > 3) {
            $this->postcode =
                substr($val, 0, 3) .
                ' ' .
                substr($val, 3);
        } else {
            $this->postcode = $val;
        }

        return $this;
    }

    /**
     * @see CheckoutAddress::$postcode
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @see CheckoutAddress::$country
     *
     * @param string $val
     *
     * @return self
     *
     * @throws ApiDataException
     */
    public function setCountry(
        string $val
    ): self {
        if ($val !== 'SE' && $val !== 'NO') {
            throw new ApiDataException(
                $val . ' is not a valid country.'
            );
        }

        $this->country = $val;

        return $this;
    }

    /**
     * @see CheckoutAddress::$country
     *
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        return [
            'firstname' => $this->firstName,
            'lastname' => $this->lastName,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'country' => $this->country,
            'street0' => $this->street0,
            'street1' => $this->street1,
            'company' => $this->company,
            'telephone' => $this->telephone,
            'countryId' => $this->countryId,
        ];
    }
}
