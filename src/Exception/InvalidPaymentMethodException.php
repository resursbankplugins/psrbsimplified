<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Exception;

use PrestaShopException;

/**
 * Indicates a problem with the order handler.
 * @since 1.0.0
 */
class InvalidPaymentMethodException extends PrestaShopException
{
}
