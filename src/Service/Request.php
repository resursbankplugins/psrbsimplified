<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use function is_bool;
use function is_string;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Exception\MissingRequestParameterException;

/**
 * Handle request data.
 */
class Request
{
    /**
     * Module context.
     *
     * @var Context
     *
     * @since 1.0.0
     */
    private $context;

    /**
     * @var ValidateGovId
     */
    private $validateGovId;

    /**
     * @var ValidatePhoneNumber
     */
    private $validatePhoneNumber;

    /**
     * @param ValidateGovId $validateGovId
     * @param ValidatePhoneNumber $validatePhoneNumber
     */
    public function __construct(
        ValidateGovId $validateGovId,
        ValidatePhoneNumber $validatePhoneNumber
    ) {
        $this->validateGovId = $validateGovId;
        $this->validatePhoneNumber = $validatePhoneNumber;
        $this->context = Context::getContext();
    }

    /**
     * Retrieve the is_company request parameter as a bool.
     *
     * @return bool
     *
     * @throws MissingRequestParameterException
     */
    public function isCompany(): bool
    {
        $result = Tools::getIsset('is_company') ?
            Tools::getValue('is_company') :
            null;

        if ($result === 'true') {
            $result = true;
        } elseif ($result === 'false') {
            $result = false;
        }

        if (!is_bool($result)) {
            throw new MissingRequestParameterException(
                'Parameter [is_company] was not set or isn\'t a bool.'
            );
        }

        return $result;
    }

    /**
     * Validates and returns identifier value utilised to fetch address.
     *
     * @param bool $isCompany
     *
     * @return string
     *
     * @throws InvalidDataException
     * @throws MissingRequestParameterException
     */
    public function getIdentifier(
        bool $isCompany
    ): string {
        $result = Tools::getValue('identifier');

        if (!is_string($result)) {
            throw new MissingRequestParameterException(
                __('Parameter [identifier] was not set or isn\'t a string.')
            );
        }

        $country = $this->context->country->iso_code;

        if ($country === 'SE' &&
            !$this->validateGovId->sweden($result, $isCompany)
        ) {
            throw new InvalidDataException(
                __('Invalid Swedish government ID.')
            );
        } elseif ($country === 'NO' &&
            !$this->validatePhoneNumber->norway($result)
        ) {
            throw new InvalidDataException(
                __('Invalid phone number.')
            );
        }

        return $result;
    }
}
