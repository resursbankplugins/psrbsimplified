<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
/* @noinspection PhpCastIsUnnecessaryInspection */
/* @noinspection PhpComposerExtensionStubsInspection */
/* @noinspection PhpMultipleClassDeclarationsInspection */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use Country;
use Exception;
use http\Exception\InvalidArgumentException;
use function in_array;
use function is_array;
use function is_object;
use JsonException;
use PrestaShop\PrestaShop\Adapter\Entity\Address;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Translate;
use PrestaShop\PrestaShop\Core\Payment\PaymentOption;
use psrbsimplified;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Exception\PaymentMethodException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Service\PaymentMethods;
use Resursbank\Core\Service\ReadMore;
use Resursbank\Ecommerce\Service\Translation;
use Resursbank\RBEcomPHP\ResursBank;
use SmartyException;

/**
 * Resurs Bank handler for Checkout based actions (before the purchase). This
 * for example renders customer data and payment methods when the customer is
 * still in the initial "after cart"-state.
 *
 * @since 1.0.0
 */
class Checkout
{
    /**
     * @var array
     *
     * @since 1.0.0
     */
    private $fieldTranslations = [];

    /**
     * @var Logger
     *
     * @since 1.0.0
     */
    private $log;

    /**
     * Resurs Bank payment methods storage.
     *
     * @var PaymentMethods
     *
     * @since 1.0.0
     */
    private $paymentMethods;

    /**
     * Module context.
     *
     * @var Context
     *
     * @since 1.0.0
     */
    private $context;

    /**
     * The main module.
     *
     * @var psrbsimplified
     *
     * @since 1.0.0
     */
    private $module;

    /**
     * @var Session
     *
     * @since 1.0.0
     */
    private $session;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ReadMore
     */
    private $readMore;

    /**
     * @since 1.0.0
     *
     * @var ResursBank
     */
    private $apiClass;

    /**
     * @param Logger $logger
     * @param PaymentMethods $paymentMethods
     * @param Session $session
     * @param Config $config
     * @param ReadMore $readMore
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function __construct(
        Logger $logger,
        PaymentMethods $paymentMethods,
        Session $session,
        Config $config,
        ReadMore $readMore
    ) {
        $this->log = $logger;
        $this->paymentMethods = $paymentMethods;
        $this->session = $session;
        $this->config = $config;
        $this->context = Context::getContext();
        $this->readMore = $readMore;
    }

    /**
     * Set up and prepare the module.
     *
     * @param psrbsimplified $module
     *
     * @return $this
     *
     * @since 1.0.0
     */
    public function setModule(psrbsimplified $module): Checkout
    {
        $this->module = $module;

        return $this;
    }

    /**
     * @return ResursBank
     *
     * @throws Exception
     */
    private function getApiFunctions(): ResursBank
    {
        if ($this->apiClass instanceof ResursBank) {
            return $this->apiClass;
        }

        return $this->apiClass = new ResursBank();
    }

    /**
     * @param ResursbankPaymentMethod $method
     *
     * @return string
     *
     * @throws JsonException
     * @throws Exception
     */
    public function getPaymentOptionLogoUrl(ResursbankPaymentMethod $method): string
    {
        $logo = '';

        $ecomFunctions = $this->getApiFunctions();

        if ($ecomFunctions->isTrustly($method->getSpecificType(), $method->getType())) {
            $logo = 'trustly.svg';
        } elseif ($ecomFunctions->isSwish($method->getSpecificType(), $method->getType())) {
            $logo = 'swish.png';
        } elseif ($ecomFunctions->isPspCard($method->getSpecificType(), $method->getType())) {
            $logo = 'card.svg';
        } elseif ($ecomFunctions->isInternalMethod($method->getType())) {
            $logo = 'logo.png';
        }

        return $this->context->link->getMediaLink(
            $this->module->getPathUri() . 'assets/img/' . $logo
        );
    }

    /**
     * Simplify checking if customer is of type company.
     *
     * @param Address $customer
     *
     * @return bool
     *
     * @since 1.0.0
     */
    private function isCompany(Address $customer): bool
    {
        $isCompany = $this->session->getIsCompany();

        return $this->session->getIsCompany() ||
            !empty($customer->company) ||
            !empty($customer->vat_number);
    }

    /**
     * Check if payment amount is within allowed limits of payment method.
     * Borrowed from ecomphp with a few modifications.
     *
     * @param float $totalAmount
     * @param float $minAmount
     * @param float $maxAmount
     *
     * @return bool
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function getMinMax(
        float $totalAmount,
        float $minAmount,
        float $maxAmount
    ): bool {
        return $this->getApiFunctions()->getMinMax($totalAmount, $minAmount, $maxAmount);
    }

    /**
     * Render payment methods for the customer checkout (normally step 4).
     *
     * @return array
     *
     * @throws PaymentMethodException|SmartyException
     * @throws Exception
     * @throws JsonException
     * @throws PaymentMethodException
     * @throws SmartyException
     *
     * @since 1.0.0
     * @see https://test.resurs.com/docs/x/doCJAw
     */
    public function getPaymentOptions(): array
    {
        $optionArray = [];
        $list = $this->paymentMethods->getList();
        $address = $this->getCustomerAddress($this->getCustomerId());

        $realIndex = -1;
        /** @var ResursbankPaymentMethod $method */
        foreach ($list as $index => $method) {
            // Validate allowed customer types with what's entered in the order forms.
            // Also validate if the payment method is allowed with the current order total.
            if (!$this->isAllowedPaymentMethod($method)) {
                continue;
            }
            $realIndex++;

            $actionUrl = $this->module->getModuleLink('validate');

            /**
             * When we add <section>-tags to our custom form, PrestaShop is failing to render the rest of the
             * checkout (meaning: addHiddenSubmitButton will not work). This error can either be turned off
             * by running libxml_use_internal_errors(true) - or simply just not add that element to the form.
             *
             * @see https://stackoverflow.com/questions/6090667/php-domdocument-errors-warnings-on-html5-tags
             */
            $formFields = $this->getCustomerDataFormFields($method);
            $infoLabel = $this->getMethodInfo($method);
            $infoDetailsTest = $this->getMethodInfo($method, '2');
            // Extended info is a long informative text that could be applied in descriptions, but we should
            // normally not do this.
            $extendedInfo = $this->getMethodInfo($method, 'extendedInfo');
            $infoDetails = '';
            // If condition area to extend information on demand. If infoTextId2 exists in the translation tables.
            if (!empty($infoDetailsTest) && $infoDetailsTest !== $infoLabel) {
                $infoDetails = $infoDetailsTest;
            }
            $this->context->smarty->assign(array_merge(
                compact('actionUrl', 'formFields', 'infoLabel', 'infoDetails', 'extendedInfo'),
                $this->readMore->getTemplateData(
                    $this->config->getReadMoreTranslatedString($method),
                    '',
                    'readmore-' . $method->getCode(),
                    $method,
                    $infoLabel,
                    $infoDetails,
                    0.0,
                    false
                ),
                [
                    'isLast' => $index === count($list) - 1,
                    'isFirst' => $realIndex === 0,
                    'country' => $this->getCustomerCountry(),
                    'phone' => $this->getCustomerPhone(),
                    'isCompany' => $this->isCompany($address),
                ]
            ));

            $paymentForm = $this->getSmartyTemplate('paymentform.tpl');
            $externalOption = new PaymentOption();
            $externalOption
                ->setModuleName($this->module->name)
                ->setCallToActionText($method->getDescription())
                ->setLogo(
                    $this->getPaymentOptionLogoUrl($method)
                )
                ->setAction(
                    $actionUrl
                )->setForm(
                    $paymentForm
                );

            $optionArray[] = $externalOption;
        }

        if (!count($optionArray)) {
            $this->log->info(
                sprintf(
                    'Function %s did not render any payment methods for Resurs Bank.',
                    __FUNCTION__
                )
            );
        }

        return $optionArray;
    }

    /**
     * @param ResursbankPaymentMethod $method
     *
     * @return bool
     *
     * @throws Exception
     *
     * @since 1.0.0
     * @noinspection PhpCastIsUnnecessaryInspection
     */
    private function isAllowedPaymentMethod(
        ResursbankPaymentMethod $method
    ): bool {
        /** @noinspection ArgumentEqualsDefaultValueInspection */
        $orderTotal = (float) $this->context->cart->getOrderTotal(true);
        $customer = $this->getCustomerAddress((int) $this->context->cart->id_address_invoice);
        $return = in_array(
            $this->isCompany($customer) ? 'LEGAL' : 'NATURAL',
            $method->getCustomerType(),
            true
        );

        if (!$this->getMinMax(
            $orderTotal,
            $method->getMinOrderTotal(),
            $method->getMaxOrderTotal())
        ) {
            $return = false;
        }

        // Check for a properly instantiated Country and decide what to show.
        if ($this->context->country instanceof Country) {
            $country = $this->getCustomerCountry();

            // PSP cards are global. If the CARD is not present for the PSP, and customer country is different
            // to the shop's, this method should not be shown. As long as we can't get the customer country
            // from a selected form field, this value will be empty and therefore not necessary to look up.
            if (!empty($country) && $country !== $this->config->getShopAddressCountry()) {
                // Payment method should not show up when not set to store country.
                // If default country is set to anything but the API-country this may not work properly.
                $return = false;

                // Unless this is a PSP-CARD. "CARD" does not always appear first in the type string
                // and can be both CARD, DEBIT_CARD and CREDIT_CARD as an example.
                if (strtoupper($method->getSpecificType()) === 'PAYMENT_PROVIDER' &&
                    preg_match('/CARD/i', strtoupper($method->getType()))
                ) {
                    $return = true;
                }
            }
        }

        return $return;
    }

    /**
     * Pass all template fetching through this method.
     * Great to use when we start migrating away from smarty.
     *
     * @param string $templateName
     *
     * @return string
     *
     * @throws SmartyException
     *
     * @since 1.0.0
     */
    private function getSmartyTemplate(string $templateName): string
    {
        return $this->context->smarty->fetch(
            $this->getFrontTemplateFileName($templateName)
        );
    }

    /**
     * Get form fields for simplified flow based on customer type. Put in its own method as this control
     * may expand further based on other data.
     *
     * Note: With MerchantAPI, both customer types are allowed if the payment method is empty, but this should
     * be a thing for ecomphp to be solved.
     *
     * @param ResursbankPaymentMethod $method
     * @param bool $isCompany
     *
     * @return array
     *
     * @throws JsonException
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getRequiredFormByCustomerType(ResursbankPaymentMethod $method, bool $isCompany): array
    {
        return $isCompany && in_array('LEGAL', $method->getCustomerType(), true) ?
            $this->getSimplifiedRequiredFields($method->getId(), 'LEGAL') :
            $this->getSimplifiedRequiredFields($method->getId());
    }

    /**
     * Customer payment form renderer. Data in this section is not necessary to
     * be shown but is still in the current state showing for test purposes.
     *
     * @param ResursbankPaymentMethod $method
     *
     * @return string
     *
     * @throws SmartyException
     * @throws Exception
     *
     * @since 1.0.0
     * @see https://resursbankplugins.atlassian.net/browse/P17-71
     * @noinspection SpellCheckingInspection
     * @noinspection PhpCastIsUnnecessaryInspection
     */
    private function getCustomerDataFormFields(ResursbankPaymentMethod $method): string
    {
        $return = '';

        /* @noinspection PhpCastIsUnnecessaryInspection */
        if (isset($this->context->smarty) &&
            is_object($this->context->smarty) &&
            (int) $this->context->cart->id_address_invoice
        ) {
            $paymentMethodFields = '';

            $customer = $this->getCustomerAddress((int) $this->context->cart->id_address_invoice);

            $invoiceId = (int) $this->context->cart->id_address_invoice;
            $deliveryId = (int) $this->context->cart->id_address_delivery;
            $formFieldRequest = $this->getRequiredFormByCustomerType($method, $this->isCompany($customer));
            $contextCustomer = $this->context->customer;

            $variables = $this->getDefaultVariables();
            // Prepare data for form table, include information if invoice address id is the same as the delivery
            // address, as this indicates that the customer is paying for the order for the same address.
            $variables = array_merge(
                $variables,
                [
                    'formId' => $method->getId() . '-payment-form',
                    'methodDesc' => $method->getDescription(),
                    'id' => $method->getId(),
                    'shopId' => $this->context->shop->id,
                    'billingAndShippingIsSame' => $invoiceId > 0 && $invoiceId === $deliveryId,
                    'isCompany' => $this->isCompany($customer),
                ]
            );

            // The current ecom module is very much still in trust of an older flow, for where applicant_XXX-data
            // was the way to separate NATURAL from LEGAL. They are no longer needed the same way as before, so we
            // are correcting this on the fly, removes the obsolete variables that ecom gives us (which should probably
            // be patached away in a future release of ecom) - and instead ad a mergable variable in the form of
            // phone only.
            if (!in_array('phone', $formFieldRequest, true)) {
                $formFieldRequest[] = 'phone';
            }
            $formFieldRequest = array_diff(
                $formFieldRequest,
                [
                    'mobile',
                    'applicant_telephone_number',
                    'applicant_mobile_number',
                ]
            );

            foreach ($formFieldRequest as $fieldName) {
                $fieldValue = '';
                $fieldVisible = false;
                // When fields are empty, we usually show them in the checkout forms.
                // This variable will override the usual behaviour and is mainly used for
                // government id data together with PAYMENT_PROVIDER.
                $hideField = false;
                switch ($fieldName) {
                    case 'applicant_full_name':
                        $fieldValue = $this->getCustomerFullName();
                        break;
                    case 'applicant_mobile_number':
                    case 'mobile':
                    case 'applicant_telephone_number':
                    case 'phone':
                        // Use phone for phone. If not exists, get from mobile.
                        $fieldValue = $this->getCustomerPhone();
                        $fieldVisible = false;
                        break;
                    case 'applicant_email_address':
                    case 'email':
                        // Customer email from context should be expected if it does not exist in the current
                        // cart-customer. If there is no email address at all, we will end up in an empty
                        // value and problems with bookPayment.
                        $contextCustomerFailover = $contextCustomer->email ?? '';
                        $fieldValue = isset($customer->email) && !empty($customer->email) ?
                            $customer->email : $contextCustomerFailover;
                        break;
                    case 'applicant_government_id':
                        // LEGAL
                        $fieldValue = $this->session->getGovId();
                        break;
                    case 'government_id':
                        // NATURAL
                        $fieldValue = $this->session->getGovId();
                        if ($method->getType() === 'PAYMENT_PROVIDER') {
                            $fieldVisible = false;
                            $hideField = true;
                        }
                        break;
                    default:
                }

                $variables['fieldName'] = sprintf(
                    '%s_%s',
                    $fieldName,
                    $method->getId()
                );
                $variables['fieldLabel'] = $this->getFieldString($fieldName);
                $variables['fieldValue'] = $fieldValue;
                $variables['hideField'] = $hideField;
                $variables['fieldVisible'] = $fieldVisible;
                $this->context->smarty->assign($variables);
                $paymentMethodFields .= $this->getSmartyTemplate('formfields.tpl');
            }

            $variables = ['fields' => $paymentMethodFields];
            $this->context->smarty->assign($variables);
            $return = $this->getSmartyTemplate('paymentmethodform.tpl');
        }

        return $return;
    }

    /**
     * @return string
     *
     * @throws SmartyException
     */
    public function getFetchAddressField(): string
    {
        $env = $this->config->getEnvironment();

        $this->context->smarty->assign([
            'shopCountry' => $this->config->getCountry(),
            'hasCredentials' => (
                $this->config->getUsername($env) !== '' &&
                $this->config->getPassword($env) !== ''
            ),
        ]);

        return $this->context->smarty->fetch(
            $this->getFrontTemplateFileName('fetchaddressfield.tpl')
        );
    }

    /**
     * Get customer phone number from a customer Address object (through stored session).
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCustomerPhone(): string
    {
        return $this->getAddressData(
            $this->getCustomerAddress($this->getCustomerId()),
            'phone'
        );
    }

    /**
     * Get customer full name for the API by merging first and last name into one string.
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCustomerFullName(): string
    {
        return sprintf(
            '%s %s',
            $this->getAddressData(
                $this->getCustomerAddress($this->getCustomerId()),
                'firstname'
            ),
            $this->getAddressData(
                $this->getCustomerAddress($this->getCustomerId()),
                'lastname'
            )
        );
    }

    /**
     * Get customer country code (ISO) from shop forms.
     * An empty string is returned when data is not yet filled in.
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCustomerCountry(): string
    {
        $countryId = (int) $this->getAddressData(
            $this->getCustomerAddress($this->getCustomerId()),
            'id_country'
        );

        return $countryId ? Country::getIsoById($countryId) : '';
    }

    /**
     * Get a specific address value from an Address object.
     * Some methods very much relies on getAddressData for where we now can extract both from objects and arrays.
     *
     * @param Address $address
     * @param string $key
     *
     * @return string
     *
     * @since 1.0.0
     */
    private function getAddressData(Address $address, string $key): string
    {
        return $address->{$key} ?? '';
    }

    /**
     * Get preferred customer id via current cart.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCustomerId(): int
    {
        // If there is an id for the invoice, this is where our live data
        // resides for the current cart and order. If not, fall back to the
        // default context customer.
        /** @noinspection PhpCastIsUnnecessaryInspection */
        $idInvoice = (int) $this->context->cart->id_address_invoice;

        /** @noinspection PhpCastIsUnnecessaryInspection */
        $idCustomer = (int) $this->context->customer->id;

        return $idInvoice ?: $idCustomer;
    }

    /**
     * Get customer address by customer id.
     *
     * @param int $customerId
     *
     * @return Address
     *
     * @since 1.0.0
     */
    private function getCustomerAddress(int $customerId): Address
    {
        return new Address($customerId);
    }

    /**
     * Get an array with fields that is required for customer to fill in, in the checkout.
     *
     * @param string $methodSpecificType
     * @param string $customerType
     *
     * @return array
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function getSimplifiedRequiredFields(string $methodSpecificType, string $customerType = 'NATURAL'): array
    {
        if ($customerType !== 'NATURAL' && $customerType !== 'LEGAL') {
            throw new InvalidArgumentException('customerType must be of type NATURAL or LEGAL');
        }

        return $this->getApiFunctions()->getSimplifiedRequiredFields($methodSpecificType, $customerType);
    }

    /**
     * Returns an array with defaults, that are injected into the payment method field template.
     *
     * @return array
     *
     * @since 1.0.0
     */
    private function getDefaultVariables(): array
    {
        return [
            'fieldName' => '',
            'fieldLabel' => '',
            'displayMode' => '',
            'fieldSize' => 32,
            'fieldValue' => '',
        ];
    }

    /**
     * Returns default translation.
     *
     * @param string $key
     *
     * @return string|null
     *
     * @since 1.0.0
     * @see https://test.resurs.com/docs/display/ecom/General%3A+Integration+development#General:Integrationdevelopment-Thecheckoutandformsthatneedstobefilledin
     */
    private function getFieldString(string $key): ?string
    {
        // Note: There are fields that are getting obsolete, but partially still in use. You can see which
        // from the link above.
        if (empty($this->fieldTranslations)) {
            $this->fieldTranslations = [
                'government_id' => $this->module->l('Social security number'),
                'phone' => $this->module->l('Phone number'),
                'mobile' => $this->module->l('Mobile number'),
                'email' => $this->module->l('E-mail address'),
                'card_number' => $this->module->l('Card number'),
                'contact_government_id' => $this->module->l('Contact government id'),
                'applicant_government_id' => $this->module->l('Applicant government id'),
                'applicant_full_name' => $this->module->l('Applicant Full Name'),
                'applicant_telephone_number' => $this->module->l('Applicant Phone Number'),
                'applicant_mobile_number' => $this->module->l('Applicant Mobile Number'),
                'applicant_email_address' => $this->module->l('Applicant E-Mail'),
            ];
        }

        return $this->fieldTranslations[$key] ?? '';
    }

    /**
     * Fetch the template path and filename for a template.
     *
     * @param string $templateName
     *
     * @return string
     *
     * @since 1.0.0
     *
     * @todo Use twig!
     */
    private function getFrontTemplateFileName(string $templateName): string
    {
        $template = sprintf('%s/%s', psrbsimplified::templatePath(), $templateName);

        return file_exists($template) ? $template : '';
    }

    /**
     * Resolve additional method information, if any.
     *
     * @param ResursbankPaymentMethod $method
     * @param string $infoTextId infoTextId (not as integer as we may want to use other kind of keys too)
     *
     * @return string
     */
    private function getMethodInfo(
        ResursbankPaymentMethod $method,
        string $infoTextId = '1'
    ): string {
        $result = '';

        try {
            $trans = new Translation();
            $trans->setLanguage($this->getCustomerCountry());

            /*
             * Fetching infoTextN for specificType, type and specificType by "PSP"-terms.
             * This may be a good idea to instead enhance from ecom's point of view where data extraction
             * is based on several keywords, instead of having the logics on plugin level.
             *
             * For exact information on the extraction, please see the following link:
             *
             * @see https://test.resurs.com/docs/x/-IBHB#EComPHPandlocalization-Current(May2022)translationstringbuildup
             */
            $bySpecificType = $trans->getMethodInfo($method->getSpecificType(), $infoTextId);
            $byType = $trans->getMethodInfo($method->getType(), $infoTextId);
            $bySpecificPspType = $trans->getMethodInfo(sprintf('psp%s', $method->getSpecificType()), $infoTextId);
            $byInternal = $trans->getPhraseByMethod(sprintf('resurs_%s', $method->getSpecificType()), 'subTitle');

            if ($this->getApiFunctions()->isInternalMethod($method->getType()) &&
                !empty($byInternal) && !is_array($byInternal)
            ) {
                $result = $byInternal;
            } elseif ($this->getApiFunctions()->isTrustly($method->getSpecificType(), $method->getType())) {
                $result = $trans->getPhraseByMethod('pspTrustly', 'info');
            } elseif (!empty($bySpecificPspType) && !is_array($bySpecificPspType)) {
                $result = $bySpecificPspType;
            } elseif (empty($bySpecificType) && !empty($byType) && !is_array($byType)) {
                $result = $byType;
            } else {
                $result = $bySpecificType;
            }

            // When a value is not directly found in the translation object, the whole method object are returned
            // instead. This allows deeper searching for more phrases yet to be discovered. But in this local
            // case, we can not return that as a string, so it has to been "zeroed" out first.
            if (is_array($result)) {
                $result = $result[$infoTextId] ?? '';
            }
        } catch (Exception $e) {
            // Suppress Exceptions. Doesn't matter here.
        }

        return $result;
    }
}
