<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use function preg_match;

/**
 * Validates phone numbers for countries that use them as a government id.
 */
class ValidatePhoneNumber
{
    /**
     * Validates a Norwegian phone number.
     *
     * @param string $val
     *
     * @return bool
     */
    public function norway(
        string $val
    ): bool {
        /* @noinspection NotOptimalRegularExpressionsInspection */
        return (bool) preg_match(
            '/^(\+47|0047|)?[ |-]?[2-9]([ |-]?[0-9]){7,7}$/',
            $val
        );
    }
}
