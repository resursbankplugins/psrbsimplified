<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use Cart;
use CartRule;
use Configuration;
use Doctrine\DBAL\Exception\TableNotFoundException;
use Exception;
use JsonException;
use OrderHistory;
use PrestaShop\PrestaShop\Adapter\Entity\Customer;
use PrestaShop\PrestaShop\Adapter\Entity\ModuleFrontController;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use PrestaShop\PrestaShop\Adapter\Entity\Translate;
use PrestaShop\PrestaShop\Adapter\Entity\Validate;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use PrestaShopDatabaseException;
use PrestaShopException;
use RESURS_EXCEPTIONS;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Model\OrderStates;
use Resursbank\Core\Service\Order as OrderService;
use Resursbank\Simplified\Exception\ApiException;
use Resursbank\Simplified\Exception\CartRebuildException;
use Resursbank\Simplified\Exception\EmptyPhoneNumberException;
use Resursbank\Simplified\Exception\InvalidGovIdException;
use Resursbank\Simplified\Exception\InvalidPaymentMethodException;
use Resursbank\Simplified\Exception\OrderDeniedException;
use Resursbank\Simplified\Exception\OrderHandlerException;
use Resursbank\Simplified\Exception\SimplifiedConstants;
use ResursException;
use SoapFault;
use TorneLIB\Exception\ExceptionHandler;

class OrderHandler extends ModuleFrontController
{
    /**
     * Cart container we use to validate our data through.
     *
     * @var Cart
     *
     * @since 1.0.0
     */
    private $simplifiedCart;

    /**
     * Customer container we use to validate our customer data through (and the
     * stuff we will use to create the Resurs Order).
     *
     * @var Customer
     *
     * @since 1.0.0
     */
    protected $customer;

    /**
     * The order container where we fetch necessary data for the order creation.
     *
     * @var Order
     *
     * @since 1.0.0
     */
    protected $order;

    /**
     * @var Api
     *
     * @since 1.0.0
     */
    protected $api;

    /**
     * Logger, public, since we use it from validate.php too.
     *
     * @var Logger
     *
     * @since 1.0.0
     */
    public $logger;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @param Api $api
     * @param Logger $logger
     * @param OrderService $orderService
     */
    public function __construct(
        Api $api,
        Logger $logger,
        OrderService $orderService
    ) {
        parent::__construct();

        $this->api = $api;
        $this->logger = $logger;
        $this->orderService = $orderService;
    }

    /**
     * @param string $emailAddress
     *
     * @return bool
     *
     * @see https://test.resurs.com/docs/display/ecom/Customer+data+-+Regular+expressions
     */
    private function isValidResursEmail(string $emailAddress): bool
    {
        return (bool) preg_match('@^[A-Za-z0-9!#%&\'*+/=?^_`~-]+(\.[A-Za-z0-9!#%&\'*+/=?^_`~-]+)*\@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$@', $emailAddress);
    }

    /**
     * @return void
     *
     * @throws Exception
     */
    public function validateCartToOrder(): Order
    {
        $failUrl = $this->getFailUrl(true);

        try {
            if (!$this->module->active || !$this->isValidCart()) {
                $this->errors[] = $this->module->l(
                    'Unable to process order - current cart is not valid, or module is not enabled.'
                );
                $this->logger->error('Unable to process order - current cart is not valid, or module is not enabled.');
                $this->redirectWithNotifications($failUrl);
            }

            /* @var Customer customer */
            $this->customer = new Customer($this->getCartCustomerId());
            if (!Validate::isLoadedObject($this->customer)) {
                $this->redirectWithNotifications($failUrl);
            }

            /*
             * Short note: Resurs allows empty phone- and mobile numbers to be
             * sent to the API. When such cases occur, Resurs will forcefully
             * redirect customers to complete missing data in the order. We
             * consider this at the plugin side as a required field, and
             * therefore we do not allow invalid or empty phone numbers in an
             * early state.
             *
             * @see P17-305
             */
            if (!$this->hasValidPhoneNumber()) {
                throw new EmptyPhoneNumberException(
                    $this->module->l('Phone number is invalid or missing.'),
                    SimplifiedConstants::MISSING_PHONE_NUMBER
                );
            }
            if (!$this->isValidResursEmail($this->customer->email)) {
                throw new EmptyPhoneNumberException(
                    $this->module->l('E-mail address is invalid or missing.'),
                    SimplifiedConstants::MISSING_PHONE_NUMBER
                );
            }

            $this->module->validateOrder(
                $this->getCartId(),
                $this->getInitialOrderStatus(),
                0.0,
                $this->getDisplayName(),
                null,
                null,
                $this->getCurrencyId(),
                false,
                $this->getCustomerSecureKey()
            );

            $this->order = $this->getOrder();

            /*
             * We actually want to execute this from a hook in the Core module,
             * but the container is not initialized in the hook we require.
             * Initializing it manually causes PrestaShop to interpret the order
             * placement as occurring from the backoffice, which ends in an
             * error because no admin account is signed in. This is reflected as
             * a translation error in the logs (because PS attempts to resolve
             * the locale associated with the admin, which as stated above does
             * not exist in that context).
             *
             * @todo Move this to Core when we can utilize a hook for this.
             */
            $this->orderService->create($this->order);

            /*
             * Starting order creation at Resurs Bank. If we get signing or
             * failures here, this layer will handle the redirects instead of
             * the one below.
             */
            $this->createResursOrder($this->order);
        } catch (Exception $e) {
            // Errors that should be logged despite debugging disabled.
            $this->logger->exception($e, true);

            // Resurs Exceptions are collected in the notification array
            // elsewhere, so if this is not an exception from Resurs Bank
            // explicitly, it should be stored here too or errors shown on
            // screen may look weirder than necessary.
            if (!$e instanceof ResursException) {
                $this->getProperOrderException($e);
            }
        }

        // Default action. Redirects should be handled via the Checkout class
        // and the API. If we reach this point, something went wrong during that
        // session.
        $this->redirectWithNotifications($failUrl);
    }

    /**
     * @param mixed $e
     *
     * @since 1.0.0
     */
    private function getProperOrderException($e): void
    {
        switch (get_class($e)) {
            case TableNotFoundException::class:
            case EmptyPhoneNumberException::class:
            case CartRebuildException::class:
            case InvalidGovIdException::class:
            case InvalidPaymentMethodException::class:
            case OrderHandlerException::class:
            case OrderDeniedException::class:
            case ApiException::class:
                $this->errors[] = $e->getMessage();
                break;
            default:
                $this->errors[] = $this->module->l(
                    'We were unable to handle this order. Please try again or contact our support'
                );
        }
    }

    /**
     * Check if the cart and customer is valid before using it by checking all
     * requirements here.
     *
     * @return bool
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function isValidCart(): bool
    {
        $return = true;

        // Validate is very much a standalone page, so not catching exceptions
        // here would destroy the order handling entirely. This is why we are
        // silently returning as a boolean.
        try {
            if ($this->getCartId() === 0 ||
                $this->getCartCustomerId() === 0 ||
                $this->getCartDeliveryId() === 0 ||
                $this->getCartInvoiceId() === 0
            ) {
                $return = false;
            }
        } catch (Exception $e) {
            $return = false;
            $this->logger->exception($e);
        }

        return $return;
    }

    /**
     * Get the id from the current cart.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCartId(): int
    {
        return (int) $this->getFromCart('id');
    }

    /**
     * Properly validate phone number with Prestashop validation since wrong
     * phone numbers may affect PrestaShop more than Resurs.
     *
     * @return bool
     *
     * @since 1.0.0
     */
    private function hasValidPhoneNumber(): bool
    {
        $customerPhone = Tools::getValue(sprintf('phone_%s', Tools::getValue('id')));

        return !empty($customerPhone) && Validate::isPhoneNumber($customerPhone);
    }

    /**
     * This function is the same as the one located in Api.php, perhaps this can
     * be centralized in a future release.
     *
     * @param $key
     *
     * @return string
     *
     * @since 1.0.1
     */
    private function getApplicantData($key): string
    {
        return (string) Tools::getValue(sprintf('applicant_%s_%s', $key, Tools::getValue('id')));
    }

    /**
     * Get data from cart. The content is very likely mixed depending on what we
     * request from the cart.
     *
     * @param $key
     *
     * @return mixed
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getFromCart($key)
    {
        if (isset($this->getCart()->{$key})) {
            $return = $this->getCart()->{$key};
        } else {
            throw new OrderHandlerException(
                sprintf('Property "%s" of Cart does not exist.', $key),
                SimplifiedConstants::NONEXISTENT_PROPERTY_EXCEPTION
            );
        }

        return $return;
    }

    /**
     * Fetching cart aside from $this->cart, since the parent is deprecated
     * since PrestaShop 1.5.x. We use the proper one that is expected by the
     * inline docs. $cart belongs to parent and already taken.
     *
     * The reason for why this method looks like it does, is to make sure that
     * we always have a cart object, but it only has to be initialized once and
     * then reused by the other features in this part of the module.
     *
     * @return Cart
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCart(): Cart
    {
        if ($this->simplifiedCart instanceof Cart) {
            $return = $this->simplifiedCart;
        } elseif (isset($this->context->cart) && $this->context->cart instanceof Cart) {
            // If not yet predefined in this class, set simplifiedCart to
            // current cart context, so it can be reused above, with lesser
            // validations.
            $return = $this->simplifiedCart = $this->context->cart;
        } else {
            // Or fail.
            throw new OrderHandlerException(
                'Cart is not a properly set cart.',
                SimplifiedConstants::INCONSISTENT_CART_EXCEPTION
            );
        }

        return $return;
    }

    /**
     * Get the current customer id from the cart.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCartCustomerId(): int
    {
        return (int) $this->getFromCart('id_customer');
    }

    /**
     * Get the current delivery address id from the customer cart.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCartDeliveryId(): int
    {
        return (int) $this->getFromCart('id_address_delivery');
    }

    /**
     * Get the current customer invoice address id from the cart object.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCartInvoiceId(): int
    {
        return (int) $this->getFromCart('id_address_invoice');
    }

    /**
     * Get order failure URL.
     *
     * @param bool $ignoreCartId If true, ignore the cart if it is broken (I.E. non existent).
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function getFailUrl(bool $ignoreCartId = false): string
    {
        $arguments = [
            'id_cart' => 0,
        ];
        try {
            $arguments['id_cart'] = $this->getCartId();
        } catch (Exception $e) {
            // Ignoring missing cart id's normally happens after the order creation process
            // in validate.php, where we don't want to bail on exception but forward customers to
            // another page.
            if (!$ignoreCartId) {
                throw $e;
            }
            $arguments['nocart'] = true;
        }

        return $this->module->getModuleLink(
            'fail',
            $arguments
        );
    }

    /**
     * Use this status to set on an initial order.
     *
     * @return int
     *
     * @since 1.0.0
     */
    private function getInitialOrderStatus(): int
    {
        return (int) Configuration::get(OrderStates::PAYMENT_PENDING);
    }

    /**
     * Get the total price from the cart. Validation of the cart property is made form getCart().
     *
     * @return float
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getOrderTotal(): float
    {
        return $this->getCart()->getOrderTotal();
    }

    /**
     * Get the display name of the module.
     * We expect a value, but validates the content anyway.
     *
     * @return string
     *
     * @since 1.0.0
     */
    private function getDisplayName(): string
    {
        $methodDescription = Tools::getValue('method');
        // Avoid invalid display names and fall back to the payment method id if this happens.
        if (!Validate::isGenericName($methodDescription)) {
            $methodDescription = Tools::getValue('id');
        }

        return isset($this->module->displayName) &&
        empty($methodDescription) ? $this->module->displayName : $methodDescription;
    }

    /**
     * Get the current currency id.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCurrencyId(): int
    {
        if (!isset($this->context->currency->id) || (int) $this->context->currency->id === 0) {
            throw new OrderHandlerException(
                sprintf('%s exception: Currency is not set or incorrect.', __FUNCTION__),
                SimplifiedConstants::NONEXISTENT_CURRENCY_EXCEPTION
            );
        }

        return (int) $this->context->currency->id;
    }

    /**
     * Get customer secure key.
     *
     * @return string
     *
     * @since 1.0.0
     */
    private function getCustomerSecureKey(): string
    {
        return $this->customer->secure_key ?? '';
    }

    /**
     * Get the entire order that has been handled this far.
     * This request is expected to be made after validation, or it won't exist.
     *
     * @return Order
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getOrder(): Order
    {
        return new Order($this->getCurrentOrderId(), $this->getCurrentCartLanguage());
    }

    /**
     * Get current order id as an integer (not the reference nor the full order object).
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCurrentOrderId(): int
    {
        if (!isset($this->module->currentOrder)) {
            throw new OrderHandlerException(
                sprintf('%s exception: Current order is not set.', __FUNCTION__),
                SimplifiedConstants::NONEXISTENT_ORDER_EXCEPTION
            );
        }

        return (int) $this->module->currentOrder;
    }

    /**
     * Get the language id from the cart.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getCurrentCartLanguage(): int
    {
        $cart = $this->getCart();

        // We normally DO expect that id_lang IS available, but for the sake on an untrusted platform,
        // we do a safe validation of it too.
        if (!isset($cart->id)) {
            throw new OrderHandlerException(
                'id_lang in cart not set',
                SimplifiedConstants::NONEXISTENT_CART_LANGUAGE_EXCEPTION
            );
        }

        return (int) $cart->id_lang;
    }

    /**
     * Create order at Resurs Bank.
     * Continues in P17-90 to handle the response (and further over also signing).
     *
     * @param Order $order
     *
     * @throws Exception
     *
     * @since 1.0.0
     * @see https://resursbankplugins.atlassian.net/browse/P17-90
     */
    private function createResursOrder(Order $order): void
    {
        // Making sure this is set before the other parts in this method, or this will for some reason
        // be sent empty.
        $this->api->setSuccessUrl($this->getSigningSuccessUrl());
        $this->api->setFailUrl($this->getFailUrl());

        // Using setOrder here will prepare the order and the order lines.
        $this->api
            ->setPreferredOrderReference($this->getOrderReference())
            ->setOrder($order);

        try {
            // Passing $order through here, to make some updates on fly (P17-168).
            $this->api->placeOrder($order);
        } catch (ResursException $e) {
            // Enforced exception logging since we're in the API execution. We normally don't need debugging
            // activated on vital issues.
            $this->logger->exception($e, true);

            // ECom Exceptions might be a bit too much for the bookPaymentSection, so we'll try to extract
            // the base faultstring from the soapclient error.
            $this->errors[] = $this->getSoapFaultByException($e);
            throw $e;
        }
    }

    /**
     * Extract faultString from a recursive exception (requires ExceptionHandler from netCurl).
     *
     * @param ResursException $e
     *
     * @return string
     *
     * @since 1.0.0
     */
    private function getSoapFaultByException(ResursException $e): string
    {
        $return = $e->getMessage();
        $previousException = $e->getPrevious();

        if ($previousException instanceof ExceptionHandler) {
            if ($previousException->getCode() < 500 || $previousException->getCode() >= 600) {
                // Previous exceptions may be quite ugly when it comes to
                // internal errors, which is not the proper way to handle
                // errors that comes from a soapFault. We will skip the message
                // if the error code is covered by 5XX. If the code is higher,
                // something else may want to tell us something.
                $return = $previousException->getMessage();
            }
            if (method_exists($previousException, 'getPrevious')) {
                $recursedPreviousException = $previousException->getPrevious();

                if ($recursedPreviousException instanceof SoapFault &&
                    isset($recursedPreviousException->faultstring)
                ) {
                    $return = $recursedPreviousException->faultstring;
                    // Fail over recursively if possible, since userErrorMessage
                    // details are lesser than the faultString.
                    if (isset($recursedPreviousException->detail->ECommerceError->userErrorMessage)) {
                        $return = $recursedPreviousException
                            ->detail
                            ->ECommerceError
                            ->userErrorMessage;
                    }
                }
            } else {
                $return = $previousException->getMessage();
            }
        }

        return $return;
    }

    /**
     * Get and prepare order success URL for bookPayment.
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getSuccessUrl(): string
    {
        return $this->context->link->getPageLink(
            'order-confirmation',
            $this->ssl,
            $this->context->language->id,
            [
                'id_cart' => $this->getCartId(),
                'id_module' => $this->getModuleId(),
                'id_order' => $this->getCurrentOrderId(),
                'key' => $this->getCustomerSecureKey(),
            ]
        );
    }

    /**
     * Render URL based on how we render it in the successUrl, but for the
     * signing part.
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getSigningSuccessUrl(): string
    {
        return $this->module->getModuleLink(
            'signing',
            [
                'id_cart' => $this->getCartId(),
                'id_module' => $this->getModuleId(),
                'id_order' => $this->getCurrentOrderId(),
                'key' => $this->getCustomerSecureKey(),
            ]
        );
    }

    /**
     * Pass through SIGNING if required.
     *
     * @throws OrderHandlerException
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function handleCustomerSigning(): void
    {
        $id_cart = Tools::getValue('id_cart');
        $id_order = Tools::getValue('id_order');

        if (!$id_cart || !$id_order) {
            throw new OrderHandlerException(
                'Can not proceed with signing due to missing order data.',
                SimplifiedConstants::MISSING_ORDER_DATA_EXCEPTION
            );
        }

        try {
            // Re-Fetch the cart from which the customer initialized the
            // payment (since prestashop needs it in the success).
            $this->simplifiedCart = new Cart($id_cart);

            // Recreate the module context as it was in the validation script.
            /*
             * $this->order will become handy here, as P17-168 may need to
             * handle orders one more time.
             */
            $this->order = new Order($id_order);
            $this->module->currentOrder = $this->order->id;
            $this->customer = new Customer($this->getCartCustomerId());

            // What we need for the last step of signing.
            $orderReference = $this->getOrderReference();
            $this->api->setSuccessUrl($this->getSuccessUrl());
            $this->api->setFailUrl($this->getFailUrl());
            $this->api->bookSignedPayment($orderReference, $this->order);
            // If errors occur, synchronization will not pass here (expected). Putting address-synchronization
            // below this point is therefore a bad idea.
        } catch (Exception $e) {
            // Enforced error logging on signing payments.
            $this->logger->exception($e, true);
            $this->errors[] = $e->getMessage();
            // This part can throw an exception if the url to signing.php are called from the browser
            // intentionally without a valid cart session.
            $this->redirectWithNotifications(
                $this->getFailUrl()
            );
        }
    }

    /**
     * Get the id of this module.
     *
     * @return int
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getModuleId(): int
    {
        if (!isset($this->module->id)) {
            throw new OrderHandlerException(
                sprintf('%s exception: Module id not set.', __FUNCTION__),
                SimplifiedConstants::MODULE_ID_MISSING_EXCEPTION
            );
        }

        return (int) $this->module->id;
    }

    /**
     * Get the order reference (the random string of 8-10 characters).
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getOrderReference(): string
    {
        if (!isset($this->order, $this->order->reference)) {
            throw new OrderHandlerException(
                'Order not properly prepared.',
                SimplifiedConstants::NONEXISTENT_ORDER_REFERENCE_EXCEPTION
            );
        }

        return $this->order->reference;
    }

    /**
     * @param Cart $cart
     *
     * @throws OrderHandlerException
     * @throws ShopException
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function cancelResursOrderByCart(Cart $cart): void
    {
        $order = Order::getByCartId($cart->id);

        if ($order === null || !isset($order->id) || $order->id === 0) {
            throw new OrderHandlerException(
                'Cannot change state for non existing order: ' .
                'Either the order does not exist, or its id is missing.',
                SimplifiedConstants::MISSING_ORDER_ID
            );
        }

        // Check if the order exists at Resurs Bank and cancel traces of it,
        // if it does.
        $connection = $this->api->getConnection(
            ShopConstraint::shop((int) $order->id_shop)
        );
        $hasConnectivityErrors = false;

        try {
            $connection->getPayment($order->reference);
            $orderDoExist = true;
        } catch (Exception $e) {
            // Exceptions for nonexistent orders are 3, 8, or 404 at this
            // moment, but since failues at this point makes us unable to
            // cancel orders in the next step, this will also cause a skip in
            // that section.
            //
            // No logging is required here since the purpose is not to find the
            // order but find out if it is missing.
            $orderDoExist = false;
            if ($e->getCode() === RESURS_EXCEPTIONS::UNKOWN_SOAP_EXCEPTION_CODE_ZERO ||
                $e->getCode() === 1015 ||
                $e->getCode() === 1019
            ) {
                $hasConnectivityErrors = true;
            }
        }
        // If the first request got a timeout, we should not bother retrying
        // contacting Resurs Bank again a second time.
        if ($orderDoExist &&
            !$connection->hasTimeoutException() &&
            !$hasConnectivityErrors
        ) {
            $connection->cancelPayment($order->reference);
        }

        if ($order->current_state !== _PS_OS_CANCELED_) {
            $orderHistory = new OrderHistory();
            $orderHistory->id_order = $order->id;
            $orderHistory->id_employee = 0;

            $orderHistory->changeIdOrderState(_PS_OS_CANCELED_, $order, !$order->hasInvoice());
            $orderHistory->addWithemail();
            $order->current_state = _PS_OS_CANCELED_;
        }
    }

    /**
     * Notifications that is stored in sessions when passing through redirectWithNotifications.
     *
     * @throws JsonException
     *
     * @since 1.0.0
     */
    private function getPriorNotifications(): void
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
            if (isset($_SESSION['notifications'])) {
                $notifications = json_decode(
                    $_SESSION['notifications'],
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );

                if (isset($notifications['error'])) {
                    $this->errors = array_merge(
                        $this->errors,
                        $notifications['error']
                    );
                }
            }
        }
    }

    /**
     * @throws CartRebuildException|JsonException
     *
     * @since 1.0.0
     * @noinspection OffsetOperationsInspection
     */
    public function rebuildAndCancel(): void
    {
        $this->getPriorNotifications();
        $cartId = (int) Tools::getValue('id_cart');
        $noCartCheck = (bool) Tools::getValue('nocart');

        if (!$cartId) {
            // noCartCheck is a redirection point that are triggered as soon as
            // someone tries to enter validate without anything to handle.
            if ($noCartCheck) {
                $this->redirectWithNotifications($this->context->link->getPageLink('order', $this->ssl));

                return;
            }

            throw new CartRebuildException(
                'Cart id not set.',
                SimplifiedConstants::MISSING_CART_ID
            );
        }

        // Partially ported from RCO v1.6-1.7.
        $cart = new Cart($cartId);

        // Cancel prior order build from the cart that is being rebuilt.
        try {
            $this->cancelResursOrderByCart($cart);
        } catch (Exception $e) {
            // Just log this exception if it occurs. If the order has not been
            // fully processed yet, there is a slight chance that your client
            // lands here, which will cause the cart to not being rebuilt
            // properly.
            $this->logger->exception($e);
        }

        $duplicateCart = $cart->duplicate();

        if (isset($duplicateCart['cart']) && $duplicateCart['cart'] instanceof Cart) {
            $newCart = $duplicateCart['cart'];
            $this->context->cookie->id_cart = $newCart->id;
            $this->context->cart = $newCart;
            CartRule::autoAddToCart($this->context);
            $this->context->cookie->write();

            // Generating a generic error here - if nothing went wrong until
            // this point. If other errors has been discovered before this
            // point, the priority should lie within that range of problems
            // and not on the failed recreation itself. For this is - if
            // customer has chosen to cancel the order - not a real error but
            // just a notification about it.
            if (!count($this->errors)) {
                $this->errors[] = $this->module->l(
                    'Payment could not be completed. Contact customer services for more information.'
                );
            }
        } else {
            // This problem normally occurs when $order->id is not set or $order itself is null by PrestaShop.
            $this->errors[] = $this->module->l(
                'Could not properly rebuild the cart. The error has been logged.'
            );
        }

        // Redirecting home to a page note.
        $this->redirectWithNotifications(
            $this->context->link->getPageLink('order', $this->ssl)
        );
    }
}
